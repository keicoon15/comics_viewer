'use strict'

const _ = require('lodash')

class Delegate {
    constructor() {
        this.delegate = []
    }
    Add(fn) {
        this.delegate.push(fn)
    }
    Broadcast(...parms) {
        _.forEach(this.delegate, fn=>fn(parms))
    }
}
let _Delegate = new Delegate()
let keyMap = {
    65: 'Left',
    68: 'Right'
}
window.addEventListener("keydown", (e)=>{
     if(keyMap[e.keyCode] != undefined)
        _Delegate.Broadcast(e.keyCode)
}, false);

module.exports = {
    Initialize: (fn) => {
        _Delegate.Add(fn)
    },
    IsKey: (key, str) => {
        return keyMap[key] == str
    }
}