'use strict'

let imageDB = new Map()
module.exports = (filePath) => {
    return new Promise((resolve, reject) => {
        //@param, valid check
        if (filePath == undefined) reject()
        const id = filePath
        if (imageDB.has(id)) resolve(imageDB.get(id))
        let image = new Image()
        image.src = filePath
        image.onload = () => {
            imageDB.set(id, image)
            resolve(image)
        }
    })
}