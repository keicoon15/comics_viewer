'use strict'

module.exports = (canvas, ctx) => {
    let screenWidth = window.innerWidth, screenHeight = window.innerHeight
    SetCanvasSize(window.innerWidth, window.innerHeight)
    function DrawImage(i) {
        ctx.clearRect(0, 0, screenWidth, screenHeight)
        ctx.drawImage(i,
            0, 0, i.naturalWidth, i.naturalHeight,
            0, 0, screenWidth, screenHeight)
    }
    function SetCanvasSize(w, h) {
        canvas.width = w
        canvas.height = h
    }
    return {
        DrawImage,
        SetCanvasSize
    }
}