'use strict'

let canvas = document.getElementById('comicCanvas');
let ctx = canvas.getContext('2d');
///
const imageLoader = require('./imageLoader')
const canvasController = require('./canvasController')(canvas, ctx)
const inputController = require('./inputController')
///
inputController.Initialize((key)=>{
    if(inputController.IsKey(key,'Left')) {
        imageLoader('test.png').then((d) => {
            canvasController.DrawImage(d)
        })
    } else if(inputController.IsKey(key,'Right')) {
        imageLoader('test2.png').then((d) => {
            canvasController.DrawImage(d)
        })
    }
})
